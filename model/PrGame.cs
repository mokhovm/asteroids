﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsteroidGame;
using PureMVC.Patterns.Proxy;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.model
{
    /// <summary>
    /// прокси, управляющий моделью данных инры
    /// </summary>
    class PrGame : Proxy
    {
        //имя прокси
        public const string CAPTION = "PrGame";
        
        // скорость игры
        public const float GAME_SPEED = 100;

        // ширина экрана
        public const int DEF_WIDTH = 800;
        // высота экрана
        public const int DEF_HEIGHT = 600;

        // максимальное количество звезд
        public const int MAX_SMALL_STARS = 60;


        // вероятность появления аптечки
        private const float AID_PROB = 0.002f;
        // вероятность появления астеройда
        private const float ASTEROID_PROB = 6;
        // максимальное количество аптечек за волну
        private const int MAX_AID_PER_WAWE = 5;

        // список объектов на экране
        public List<BaseObject> objList;
        // игровой таймер
        private System.Windows.Forms.Timer gameTimer;
        // счетчик очков
        public int score;

        // ссылка на корабль
        public Ship ship;

        public static Random rnd;

        private int waveQty = 5;
        private int releasedQty = 0;
        private int aidQty = 0;
        

        public PrGame(object data = null) : base(CAPTION, data)
        {
            rnd = new Random();
            objList = new List<BaseObject>();
        }

        /// <summary>
        /// вызывается при регистрации прокси
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            Start();
        }


        /// <summary>
        /// вызывается при удалении прокси
        /// </summary>
        public override void OnRemove()
        {
            base.OnRemove();
            Stop();
        }

        /// <summary>
        /// страт игры
        /// </summary>
        public void Start()
        {
            InitGameTimer();
        }

        /// <summary>
        /// завершение игры
        /// </summary>
        public void Stop()
        {
            gameTimer.Stop();
            gameTimer.Tick -= OnGameTimerTick;
        }

        /// <summary>
        /// инициируем таймер игры
        /// </summary>
        private void InitGameTimer()
        {
            gameTimer = new System.Windows.Forms.Timer
            {
                Interval = 20,
                Enabled = true
            };
            gameTimer.Tick += OnGameTimerTick;
        }

        /// <summary>
        /// Отсчитывает локальное время модели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGameTimerTick(object sender, EventArgs e)
        {
            Update();
            CheckEvents();
            SendNotification(GameMessages.NOTE_GAME_TICK, null, null);
        }

        /// <summary>
        /// Проверяет вероятность возникновения игрового события.
        /// </summary>
        private void CheckEvents()
        {
            // создание аптечек
            if (aidQty < MAX_AID_PER_WAWE && rnd.NextDouble() <= AID_PROB)
            {
                objList.Add(new Aidkit().Init());
                aidQty++;
            }

            // появление новых волн
            if (GetAsteroidQty() == 0 && releasedQty == waveQty)
            {
                NextWave();
            }

            // создание астеройдов
            if (rnd.Next(100) <= ASTEROID_PROB && releasedQty < waveQty)
            {
                objList.Add(new Asteroid().Init());
                releasedQty++;
            }
        }

        /// <summary>
        /// вернет количество астеройдов в игре
        /// </summary>
        /// <returns></returns>
        protected int GetAsteroidQty()
        {
            return objList.FindAll(o => o is Asteroid).Count;
        }

        /// <summary>
        /// запускает следущую волну астеройдов
        /// </summary>
        private void NextWave()
        {
            waveQty ++;
            releasedQty = 0;
            aidQty = 0;
            SendNotification(GameMessages.NOTE_GAME_NEXT_WAVE, null, null);
        }

        /// <summary>
        /// Обновление координат объектов модели
        /// </summary>
        public void Update()
        {
            foreach (BaseObject obj in objList)
            {
                obj.UpdatePos();
                checkCollision(obj);
            }

            List<BaseObject> itemsForRemoval = objList.FindAll(o => o.IsDelete);

            foreach (BaseObject item in itemsForRemoval)
            {
                objList.Remove(item);
                ship.bullets.Remove(item as Bullet);
            }
            itemsForRemoval.Clear();
        }

        /// <summary>
        /// проверяет объект на колллизии с другими объектами
        /// </summary>
        /// <param name="obj"></param>
        private void checkCollision(BaseObject obj)
        {
            if (obj is Asteroid)
            {
                foreach (var bullet in ship.bullets)
                {
                    if (obj.Collision(bullet))
                    {
                        HitAsteroid(bullet, obj);
                    }
                }

                if (obj.Collision(ship))
                {
                    HitShip(obj);
                }
            }
            else if (obj is Aidkit)
            {
                if (obj.Collision(ship))
                {
                    HitAidkit(obj);
                }
            }
        }

        /// <summary>
        /// Поймали аптечку
        /// </summary>
        /// <param name="obj"></param>
        private void HitAidkit(BaseObject obj)
        {
            ship.DoDamage((obj as Aidkit).health);
            obj.DoDamage(-obj.health);
        }

        /// <summary>
        /// Астеройд попал в корабль
        /// </summary>
        /// <param name="obj"></param>
        private void HitShip(BaseObject obj)
        {
            ship.DoDamage(-(obj as Asteroid).health);
            obj.DoDamage(-obj.health);
        }

        /// <summary>
        /// Подбили астеройд
        /// </summary>
        /// <param name="bullet"></param>
        /// <param name="asteroid"></param>
        public void HitAsteroid(BaseObject bullet, BaseObject asteroid)
        {
            System.Media.SystemSounds.Hand.Play();
            float damage = bullet.health;
            bullet.DoDamage(-damage);
            asteroid.DoDamage(-damage); 
            if (asteroid.IsDelete) score++;
        }
    }
}
