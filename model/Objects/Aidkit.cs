﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsteroidGame;
using Asteroids.model;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids
{
    /// <summary>
    /// управляет поведением аптечки
    /// </summary>
    class Aidkit : SpriteObject
    {
        public Aidkit() : base ()
        {
            
        }

        /// <summary>
        /// Инициализируем параметры аптечки
        /// </summary>
        /// <param name="isFirst"></param>
        /// <returns></returns>
        public override BaseObject Init(bool isFirst = true)
        {
            const int MIN_SIZE = 25;
            const int MAX_SIZE = 25;
            const int MAX_SPEED = 50;

            if (isFirst)
            {
                isAlwaysOnScreen = true;
                resName = Assets.ASSET_AID;
                initImage();
            }

            pos.X = PrGame.DEF_WIDTH;
            pos.Y = PrGame.rnd.Next(0, PrGame.DEF_HEIGHT);
            int initSize = PrGame.rnd.Next(MIN_SIZE, MAX_SIZE);
            size = new SizeF(initSize, initSize);
            dir = new PointF(-MAX_SPEED, 0);
            return this;
        }
    }
}
