﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.model;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace AsteroidGame
{

    /// <summary>
    /// Описывает поведение астеройда
    /// </summary>
    class Asteroid : SpriteObject
    {
        public Asteroid()
        {
            
        }

        /// <summary>
        /// Инициализируем пераметры астеройда
        /// </summary>
        /// <param name="isFirst"></param>
        /// <returns></returns>
        public override BaseObject Init(bool isFirst = true)
        {
            const int MIN_HEALTH = 1;
            const int MAX_HEALTH = 5;
            const int MAX_SPEED = 50;

            if (isFirst)
            {
                isAlwaysOnScreen = true;
                resName = Assets.ASSET_MOON;
                initImage();
            }

            pos.X = PrGame.DEF_WIDTH;
            pos.Y = PrGame.rnd.Next(0, PrGame.DEF_HEIGHT);
            health = PrGame.rnd.Next(MIN_HEALTH, MAX_HEALTH);
            size = healthToSize();
            dir = new PointF(-MAX_SPEED, 0);
            Angle = PrGame.rnd.Next(-25, 25) / 150F;
            return this;
        }

        /// <summary>
        /// Обновляет позицию астеройда
        /// </summary>
        public override void UpdatePos()
        {
            // Изменяем реальные координаты по формуле sin суммы и cos суммы и умножаем на гипотенузу, для нахождения стороны. 
            // Гипотенуза сокращается, поэтому она нам в формуле не нужна
            dir.X = (float) (Math.Sin(_angle) * dir.Y + Math.Cos(_angle) * dir.X);
            dir.Y = (float) (Math.Cos(_angle) * dir.Y - Math.Sin(_angle) * dir.X);

            UpdatePosDef();
        }

        /// <summary>
        /// Возвращет размер астеройда согласно его здоровью
        /// </summary>
        /// <returns></returns>
        private SizeF healthToSize()
        {
            float r = health * 5f + 20;
            return new SizeF(r, r); 
        }

        /// <summary>
        /// Переопределим повреждения астеройду, так как помимо уменьшения здоровья у астеройда уменьшается размер
        /// </summary>
        /// <param name="damage"></param>
        public override void DoDamage(float damage)
        {
            base.DoDamage(damage);
            // Нужно изменить размер астеройда. Но при изменении размера его изображение будет смещаться в верхний левый угол (точку крепления).
            // Чтобы этого не происходило, нужно иззменить координаты астеройда, чтобы центр нового изображения находился в той же точке,
            // что и центр старого.
            PointF oldCenter = PosCenter;
            size = healthToSize();
            pos = new PointF(oldCenter.X - size.Width / 2, oldCenter.Y - size.Height / 2);
        }


    }
}
