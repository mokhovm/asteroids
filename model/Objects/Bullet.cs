﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace AsteroidGame
{
    /// <summary>
    /// Реализует поведение пули
    /// </summary>
    class Bullet : BaseObject
    {
        protected Bullet(PointF pos, PointF dir, SizeF size) : base(pos, dir, size)
        { 

        }

        public Bullet(PointF pos)
        {
            initPos = pos;
            Init(true);
        }

        /// <summary>
        /// Инициализируем параметры пули
        /// </summary>
        /// <param name="isFirst"></param>
        /// <returns></returns>
        public override BaseObject Init(bool isFirst = true)
        {
            health = 1;
            this.pos = initPos;
            dir = new PointF(100, 0);
            size = new SizeF(4, 1);
            return this;
        }

        /// <summary>
        /// Рисуем пулю в буффере
        /// </summary>
        /// <param name="canvas"></param>
        public override void Draw(BufferedGraphics canvas)
        {
            canvas.Graphics.DrawRectangle(Pens.OrangeRed, pos.X, pos.Y, size.Width, size.Height);
        }


        /// <summary>
        /// обновление позиции пули. Если пуля покидает экран, то уничтожим её
        /// </summary>
        public override void UpdatePos()
        {
           UpdatePosDef();
           if (!InScreen())
           {
               IsDelete = true;
           }
        }


    }
}
