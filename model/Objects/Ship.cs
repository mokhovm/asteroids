﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsteroidGame;
using Asteroids.model;
using Asteroids.view;
using PureMVC.Patterns.Facade;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids
{
    class Ship : SpriteObject
    {

        // размер эпсилон-окрестности точки назначения
        private const int EPSILON = 5;
        // размер корабля
        private const int DEF_SIZE = 50;
        // скорость корабля
        private const int DEF_SPEED = 10;
        // коэффициент торможения при подлете к точке назначения
        private const float EASE_COEF = 1.5f;
        // дистанция для начала торможения
        private const int EASE_DISTANCE = 20;
        // размер обоймы для пулей
        private const int CLIP_QTY = 5;
        // скорость перезарядки
        private const int RELOAD_MS = 200;
        // количество энергии у корабля
        private const int DEF_HEALTH = 10;
        // на сколько уменьшается прямуогольник колизий
        private const int COLLISION_REDUCE = 10;
        // начальная позиция корабля на экране
        private const int DEF_POS_X = 300;

        //  количество пустых мест в обойме
        private int emptySlotQty;
        // точка назначения для корабля
        private PointF target;
        // список пулей 
        public List<Bullet> bullets;
        // время последнего выстрела
        private DateTime lastShotTime;


        /// <summary>
        /// Чуть уменьшим прямоугольник коллизий для корабля, чтобы он не сталкивался с астеройдами пиксель в пиксель
        /// </summary>
        /// <returns></returns>
        protected override RectangleF GetCollisionRect()
        {
            return new RectangleF(pos.X + COLLISION_REDUCE, pos.Y + COLLISION_REDUCE,
                size.Width - COLLISION_REDUCE * 2, size.Height - COLLISION_REDUCE * 2);
        }

        /// <summary>
        /// Инициализируем параметры корабля
        /// </summary>
        /// <param name="isFirst"></param>
        /// <returns></returns>
        public override BaseObject Init(bool isFirst = true)
        {
            bullets = new List<Bullet>();
            health = DEF_HEALTH;
            speed = DEF_SPEED;
            pos.X = DEF_POS_X;
            pos.Y = PrGame.DEF_HEIGHT / 2;
            size = new SizeF(DEF_SIZE, DEF_SIZE);
            dir = new PointF(0, 0);
            resName = Assets.ASSET_SHIP;
            initImage();

            return this;
        }

        /// <summary>
        /// рисуем изображение в буфере игры
        /// </summary>
        public override void Draw(BufferedGraphics canvas)
        {
            base.Draw(canvas);
            // рисуем для отладки прямоугольник для коллизий
            //canvas.Graphics.DrawRectangle(Pens.Red, CollisionRect.X, CollisionRect.Y, CollisionRect.Width, CollisionRect.Height);
        }

        /// <summary>
        /// Пересчитать координаты кораблья
        /// </summary>
        public override void UpdatePos()
        {
            UpdatePosDef();
            if (GetDistance(target, PosCenter) < speed * EASE_DISTANCE)
            {
                speed /= EASE_COEF;
            }
            RectangleF centerRect = new RectangleF(PosCenter.X - EPSILON, PosCenter.Y - EPSILON, 
                EPSILON * 2, EPSILON * 2);
            if (centerRect.Contains(target.X, target.Y))
            {
                dir = new PointF();
                speed = DEF_SPEED;
            }
            
            emptySlotQty = Math.Max(0, emptySlotQty);
            CheckReload();
        }

        /// <summary>
        /// Проверяет, не перезарядились ли пули
        /// </summary>
        private void CheckReload()
        {
            TimeSpan t1 = new TimeSpan(0, 0, 0, 0, RELOAD_MS);
            TimeSpan t2 = DateTime.Now - lastShotTime;
            if ( t2 > t1) emptySlotQty--;
        }

        /// <summary>
        /// Перемещает корабль в указанные координаты
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        public void MoveTo(float X, float Y)
        {
            if (X < 0) X = 0;
            if (X > PrGame.DEF_WIDTH) X = PrGame.DEF_WIDTH - Rect.Width;
            if (Y < 0) Y = 0;
            if (Y > PrGame.DEF_HEIGHT) Y = PrGame.DEF_HEIGHT - Rect.Height;
            target = new PointF(X, Y);
            PointF vector = new PointF(X - PosCenter.X, Y - PosCenter.Y);
            float length = (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y);
            dir = new PointF(vector.X / length, vector.Y / length);

            //speed = Math.Min(DEF_SPEED, length);
            speed = Math.Min(length / DEF_SPEED, DEF_SPEED);
        }

        /// <summary>
        /// Рассчитывает дистанцию между точками
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public float GetDistance(PointF p1, PointF p2)
        {
            PointF vector = new PointF(p1.X - p2.X, p1.Y - p2.Y);
            float length = (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y);
            return length;
        }


        /// <summary>
        /// Корабль стреляет
        /// </summary>
        /// <returns></returns>
        public Bullet Shoot()
        {
            Bullet res = null;
            if (emptySlotQty < CLIP_QTY)
            {
                emptySlotQty++;
                lastShotTime = DateTime.Now;
                res = new Bullet(new PointF(PosCenter.X + 10, PosCenter.Y - 1));
                bullets.Add(res);
            }
            return res;
        }

    }
}
