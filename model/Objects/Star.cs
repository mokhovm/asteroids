﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.model;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace AsteroidGame
{
    class Star : BaseObject
    {
        public static int qty = 0;

        public Star(PointF pos, PointF dir, SizeF size) : base(pos, dir, size)
        {

        }

        public Star() : base()
        {
            
        }

        /// <summary>
        /// Инициализируем звезду
        /// </summary>
        /// <param name="isFirst"></param>
        /// <returns></returns>
        public override BaseObject Init(bool isFirst = true)
        {
            const int MAX_SIZE = 6;

            if (isFirst)
            {
                isAlwaysOnScreen = true;
                qty++;
            }

            int initSize = PrGame.rnd.Next(1, MAX_SIZE);
            pos.X = isFirst ? PrGame.rnd.Next(0, PrGame.DEF_WIDTH) : PrGame.DEF_WIDTH;
            pos.Y = isFirst ? qty * PrGame.DEF_HEIGHT / PrGame.MAX_SMALL_STARS : pos.Y;
            size = new SizeF(initSize, initSize);
            dir =  new PointF(-initSize * 1.3f, 0);
            return this;
        }

        /// <summary>
        /// рисуем звезду. Пускай она будет круглой, а не крестиком
        /// </summary>
        /// <param name="canvas"></param>
        public override void Draw(BufferedGraphics canvas)
        {
            //Game.buffer.Graphics.DrawLine(Pens.White, pos.X, pos.Y, pos.X + size.Width, pos.Y + size.Height);
            //Game.buffer.Graphics.DrawLine(Pens.White, pos.X + size.Width, pos.Y, pos.X, pos.Y + size.Height);
            canvas.Graphics.FillEllipse(Brushes.White, pos.X, pos.Y, size.Width, size.Height);
        }

        /// <summary>
        /// Рассчет новых координа звезды выполняется обычным образом
        /// </summary>
        public override void UpdatePos()
        {
            UpdatePosDef();
        }
    }
}
