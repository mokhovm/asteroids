﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.model;
using PureMVC.Patterns.Facade;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace AsteroidGame
{
    interface ICollision
    {
        bool Collision(ICollision obj);
        RectangleF CollisionRect { get; }
    }


    /// <summary>
    /// Базовый класс для объектов игры
    /// </summary>
    abstract class BaseObject : ICollision
    {
        /// <summary>
        /// Базовая настройка скорости
        /// </summary>
        const float INTERNAL_SPEED = 0.1f;

        // позиция объекта
        protected PointF pos;
        // начальная позиция объекта
        protected PointF initPos;
        // вектор движения
        protected PointF dir;
        // размер объекта
        protected SizeF size;
        // скорость объекта
        protected float speed;
        // здоровье объекта
        public float health = 1;
        // объект уничтожен?
        public bool IsDelete;
        // нужно ли пересоздавать объект, если он покидает экран
        protected bool isAlwaysOnScreen = false;
        // центра объекта
        public PointF PosCenter {
            get { return new PointF(pos.X + Rect.Width / 2, pos.Y + Rect.Height / 2);}
        }

        /// <summary>
        /// Угол поворота астероида в градусах. каждую итерацию направление движения астероида меняется на это значение. 
        /// Угол хранится в радианах, а задается через соответствующее свойство в градусах.
        /// </summary>
        protected float _angle;

        /// <summary>
        /// Свойство для задания и получения угла в градусах
        /// </summary>
        public float Angle
        {
            get
            {
                return (float)(_angle * 180f / Math.PI);
            }
            set
            {
                _angle = (float)(value * Math.PI / 180f);
            }
        }

        // прямоугольник, в который вписан объект
        public RectangleF Rect => GetRect();
        // прямоугольник столкновений
        public RectangleF CollisionRect => GetCollisionRect();

        protected BaseObject(PointF pos, PointF dir, SizeF size)
        {
            this.pos = pos;
            this.dir = dir;
            this.size = size;
            initPos = pos;
            speed = INTERNAL_SPEED;
        }

        protected BaseObject()
        {
            speed = INTERNAL_SPEED;
        }

        public abstract BaseObject Init(bool isFirst = true);

        /// <summary>
        /// Возращвет рамку объекта. Переопределяется в потомках.
        /// </summary>
        /// <returns></returns>
        protected virtual RectangleF GetRect() 
        {
           return new RectangleF(pos, size);
        }

        /// <summary>
        /// Возвращает рамку столкновений. Переопределяется в потомках.
        /// </summary>
        /// <returns></returns>
        protected virtual RectangleF GetCollisionRect()
        {
            return new RectangleF(pos, size);
        }

        /// <summary>
        /// посылает сообщение в шину сообщений
        /// </summary>
        /// <param name="note">название сообщения</param>
        /// <param name="body">содержание сообщения</param>
        /// <param name="type">тип сообщения</param>
        public void SendNotification(string note, object body = null, string type = null)
        {
            Facade.GetInstance(null).SendNotification(note, body, type);
        }
     
        /// <summary>
        /// Рисует объект в буффер
        /// </summary>
        /// <param name="canvas"></param>
        public abstract void Draw(BufferedGraphics canvas);

        /// <summary>
        /// обновляет позицию объекта
        /// </summary>
        public abstract void UpdatePos();

        /// <summary>
        /// Стандартный метод обновления позиции объекта. 
        /// Подойдет большинству объектов
        /// </summary>
        protected void UpdatePosDef()
        {
            pos.X += dir.X * speed;
            pos.Y += dir.Y * speed;
            if (isAlwaysOnScreen && !InScreen())
            {
                Init(false);
            }
        }

        /// <summary>
        /// Если рамки текущего объекта пересеклись с объектом obj, то вернет тру
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Collision(ICollision obj)
        {
            bool res = obj.CollisionRect.IntersectsWith(CollisionRect);
            return res;
        }

        /// <summary>
        /// Вернет тру, если объект не вышел за пределы экрана
        /// </summary>
        /// <returns></returns>
        public bool InScreen()
        {
            var res = (pos.X + size.Width > 0 && pos.X < PrGame.DEF_WIDTH &&
                pos.Y + size.Height > 0 && pos.Y < PrGame.DEF_WIDTH);
            return res;
        }

        /// <summary>
        /// наносит повреждене объекту
        /// </summary>
        /// <param name="damage">значение повреждений. Если будет положительное, то не ранит, а лечит объект</param>
        public virtual void DoDamage(float damage)
        {
            health += damage;
            System.Diagnostics.Debug.WriteLine($"{GetType()} health: {health}");
            SendNotification(GameMessages.NOTE_OBJECT_HEALTH_CHANGE, this);
            if (health <= 0) Die();
        }

        /// <summary>
        /// Уничтожает объект
        /// </summary>
        private void Die()
        {
            SendNotification(GameMessages.NOTE_OBJECT_DIE, this);
            IsDelete = true;
        }
        
    }
}
