﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.model;
using Asteroids.Properties;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace AsteroidGame
{
    /// <summary>
    /// Класс для работы со спрайтами
    /// </summary>
    abstract class SpriteObject :BaseObject
    {
        /// <summary>
        /// изображение
        /// </summary>
        protected Image img;
        /// <summary>
        /// Название ресурса с изображением
        /// </summary>
        protected string resName;

        public SpriteObject(PointF pos, PointF dir, SizeF size, string resName) : base(pos, dir, size)
        {
            this.pos = pos;
            this.dir = dir;
            this.size = size;
            this.resName = resName;
        }

        protected SpriteObject() : base()
        {
            
        }

        /// <summary>
        /// инициализируем изображение из ресурса
        /// </summary>
        protected void initImage()
        {
            if (resName == "") throw new GameObjectException("Имя ресурса не может быть пустым.");
            var resource = Resources.ResourceManager.GetObject(resName);
            img = (Image)resource;
            if (img == null) throw new GameObjectException("Изображение не найдено.");
        }


        /// <summary>
        /// Инициализируем параметры 
        /// </summary>
        /// <param name="isFirst"></param>
        /// <returns></returns>
        public override BaseObject Init(bool isFirst = true)
        {
            if (isFirst)
            {
                initImage();
                // если указаны размеры изображения, то оно будет отмасштабировано до этих размеров
                // при выводе в буффер. В противном случае будут использоваться начальные размеры изображения
                if (size.Width == 0) size.Height = img.Height;
                if (size.Height == 0) size.Width = img.Width;
            }
            pos.X = PrGame.DEF_WIDTH;
            pos.Y = initPos.Y;
            return this;
        }

        /// <summary>
        /// рисуем изображение в буфере игры
        /// </summary>
        public override void Draw(BufferedGraphics canvas)
        {
            canvas.Graphics.DrawImage(img, Rect);
        }

        /// <summary>
        /// обновляем координаты объекта
        /// </summary>
        public override void UpdatePos()
        {
            UpdatePosDef();
        }

        /// <summary>
        /// Случайным образом перевернет картинку
        /// </summary>
        public void RndRotare()
        {
            Array values = Enum.GetValues(typeof(RotateFlipType));
            Random random = new Random();
            RotateFlipType rotare = (RotateFlipType)values.GetValue(random.Next(values.Length));
            img.RotateFlip(rotare);

        }
    }
}
