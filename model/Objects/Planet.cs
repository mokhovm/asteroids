﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsteroidGame;
using Asteroids.model;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids
{
    class Planet : SpriteObject
    {

        /// <summary>
        /// Инициализируем параметры планеты
        /// </summary>
        /// <param name="isFirst"></param>
        /// <returns></returns>
        public override BaseObject Init(bool isFirst = true)
        {
            const int MIN_SIZE = 150;
            const int MAX_SIZE = 200;
            const int MAX_SPEED = 10;

            if (isFirst)
            {
                isAlwaysOnScreen = true;
                resName = Assets.ASSET_PLANET2;
                initImage();
            }

            //pos.X = isFirst ? PrGame.rnd.Next(0, PrGame.DEF_WIDTH) : PrGame.DEF_WIDTH;
            pos.X = PrGame.DEF_WIDTH;
            pos.Y = PrGame.rnd.Next(0, PrGame.DEF_HEIGHT);
            int initSize = PrGame.rnd.Next(MIN_SIZE, MAX_SIZE);
            size = new SizeF(initSize, initSize);
            dir = new PointF(-MAX_SPEED, 0);
            return this;
        }

        /// <summary>
        /// Рассчет новых координа планеты выполняется обычным образом
        /// </summary>
        public override void UpdatePos()
        {
            UpdatePosDef();
        }


        /// <summary>
        /// прорисовываем планету. 
        /// </summary>
        /// <param name="canvas"></param>
        public override void Draw(BufferedGraphics canvas)
        {
            canvas.Graphics.DrawImage(img, Rect);

            // изменение цвета пока отключаем
            /*
            ImageAttributes imageAttributes = new ImageAttributes();
            int width = img.Width;
            int height = img.Height;

            float[][] colorMatrixElements = {
                new float[] {1,  0,  0,  0,  0},        // red scaling factor of 2
                new float[] {0,  1,  0,  0,  0},        // green scaling factor of 1
                new float[] {0,  0,  1,  0,  0},        // blue scaling factor of 1
                new float[] {0,  0,  0,  1,  0},        // alpha scaling factor of 1
                new float[] {0.2f, .2f, .2f, 0, 1}};    // three translations of 0.2

            ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);

            imageAttributes.SetColorMatrix(
                colorMatrix,
                ColorMatrixFlag.Default,
                ColorAdjustType.Bitmap);

            
            
            PrGame.buffer.Graphics.DrawImage(
                img,
                Rectangle.Round(Rect),
                0, 0,        // upper-left corner of source rectangle 
                width,       // width of source rectangle
                height,      // height of source rectangle
                GraphicsUnit.Pixel,
                imageAttributes);
            */

        }
    }
}
