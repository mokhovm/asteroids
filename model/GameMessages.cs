﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.model
{

    /// <summary>
    /// Нотификации для шины сообщений
    /// </summary>
    class GameMessages
    {
        public const string NOTE_DRAW = "/note/game/draw";

        // новый игровой квант
        public const string NOTE_GAME_TICK = "/note/game/tick";
        // новое координаты движения корабля
        public const string NOTE_SHIP_MOVE = "/note/game/shipMove";
        // корабль производит выстрел
        public const string NOTE_SHIP_SHOOT = "/note/game/shipShoot";
        // объект уничтожается (телом передается ссылка на объект)
        public const string NOTE_OBJECT_DIE = "/note/game/objectDie";
        // здороьве объекта изменилось (телом передается ссылка на объект)
        public const string NOTE_OBJECT_HEALTH_CHANGE = "/note/game/objectHealthChange";
        // игра проиграна
        public const string NOTE_GAME_LOSS = "note/game/loss";
        // следующая волна
        public const string NOTE_GAME_NEXT_WAVE = "note/game/nextWave";
        // показать сообщение (телом - ссылка на экземпляр ScreenTextVO)
        public const string NOTE_SHOW_MSG = "note/game/showMsg";

        // старт приложения
        public const string CMD_STARTUP = "command/startup";
        // останов приложения
        public const string CMD_SHUTDOWN = "command/shutdown";
        // игра начата
        public const string CMD_GAMESTART = "command/gameStart";
        // игра закончена
        public const string CMD_GAMESTOP = "command/gameStop";

        // логгирование
        public const string NOTE_LOG = "/note/log";

    }

    /// <summary>
    /// Служебный класс для передачи сообщений, выдаваемых на экране
    /// </summary>
    class ScreenTextVO
    {
        // координаты сообщения
        public PointF coord;
        // текст
        public string text;
        // какое количество миллисекунд показывается сообщение
        public int ms;
        // время начала отображения
        public DateTime StartTime;
        // нужно ли удалить сообщение
        public bool forRemove;

        public ScreenTextVO(string text, PointF coord, int ms = 2000)
        {
            this.coord = coord;
            this.text = text;
            this.ms = ms;
        }

        public ScreenTextVO(string text)
        {
            this.text = text;
            coord = new PointF(PrGame.DEF_WIDTH / 2, PrGame.DEF_HEIGHT / 2);
            ms = 2000;
        }
    }
}
