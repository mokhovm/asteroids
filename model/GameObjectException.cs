﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace AsteroidGame
{
    /// <summary>
    /// Класс исключений для игры
    /// </summary>
    class GameObjectException : Exception
    {
        public GameObjectException(string message) : base (message)
        {
        }
    }
}
