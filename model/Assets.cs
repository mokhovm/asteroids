﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.model
{
    /// <summary>
    /// названия ресурсов 
    /// </summary>
    class Assets
    {
        // солнце
        public const string ASSET_SUN = "sun";
        // астеройд
        public const string ASSET_MOON = "fobos";
        // звезда
        public const string ASSET_STAR = "star";
        // корабль
        public const string ASSET_SHIP = "ship";
        // аптечка
        public const string ASSET_AID = "aid";
        // планета 1
        public const string ASSET_PLANET1 = "planet1";
        // планета 2
        public const string ASSET_PLANET2 = "planet2";
        // планета 3
        public const string ASSET_PLANET3 = "planet3";
    }
}
