﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.model;
using Asteroids.view;
using PureMVC.Interfaces;
using PureMVC.Patterns.Command;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.controller
{
    /// <summary>
    /// Команда окончания игры
    /// </summary>
    class CmdStopGame : SimpleCommand
    {
        public override void Execute(INotification note)
        {
            SendNotification(GameMessages.NOTE_LOG, GetType().ToString(), null);
            Facade.RemoveMediator(MdGame.CAPTION);
            Facade.RemoveProxy(PrGame.CAPTION);
        }
    }
    
}
