﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Asteroids;
using PureMVC.Interfaces;

namespace AsteroidGame
{
    #region Задание для второго уровня С# 
    /*
        Игра Asteroids. ver 0.04
        Выполнил Мохов Михаил.
        mokhovm@gmail.com

        Игра написана с использованием паттерна MVC и его практической реализции в виде библиотеки PureMVC
        http://puremvc.org/

        Благодаря такому подходу удалось существенно упросить взаимодействие частей игры между собой, разнести логику игры и
        её экранное представление. Для упарвления компонентами игры применена общая шина данных, реализованная с помощью паттерна 
        Observer, а простая адресация и собвственная система нотификаций позволяют легко перенести модули игры на любой другой язык 
        программирования, поддерживающий PureMVC. Все ключевые части игры регистрируются в специальном реестре и объединяются с 
        помощью паттерна Facade. Из любой точки программы можно обратится к нужному медиатору, прокси или команде, иемя только ссылку 
        на интерфейс фасада, которая реализова с помощью синглтона. 

    */
    #endregion


    static class Program
    {
      
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // создаем форму
            Form frmMenu = new FrmMenu();
            // создаем фасад приложения 
            var facade = AsteroidFacade.MyFacade;
            // запускаем приложение
            facade.Startup(frmMenu);
            // запускаем WF application
            Application.Run(frmMenu);

        }   


    }
}
