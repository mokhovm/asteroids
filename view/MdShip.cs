﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AsteroidGame;
using Asteroids.model;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.view
{
    /// <summary>
    /// Визуализирует корабль
    /// </summary>
    class MdShip : Mediator
    {
        // название медиаторв
        public const string CAPTION = "MdShip";
        // ссылка на игровой прокси
        private PrGame prGame;
        // корабль
        private Ship Ship
        {
            get => prGame.ship;
            set => prGame.ship = value;
        }
        // буффер для рисования графики
        public BufferedGraphics buffer;

        public MdShip(BufferedGraphics aBuffer) : base(CAPTION, aBuffer)
        {
            prGame = Facade.RetrieveProxy(PrGame.CAPTION) as PrGame;
            buffer = aBuffer;
        }

        /// <summary>
        /// Вызывается при регистрации медиатора
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            Ship = new Ship().Init() as Ship;
            prGame.objList.Add(Ship);
        }

        /// <summary>
        /// Вызывается при уничтожении медиатора
        /// </summary>
        public override void OnRemove()
        {
            base.OnRemove();
        }

        /// <summary>
        /// производим выстрел
        /// </summary>
        private void Shoot()
        {
            Bullet bullet = Ship.Shoot();
            if (bullet != null)
            {
                prGame.objList.Add(bullet);
            }
        }

        /// <summary>
        /// двигаемся к указаной координате
        /// </summary>
        /// <param name="e"></param>
        private void Move(MouseEventArgs e)
        {
            Ship.MoveTo(e.X, e.Y);
        }

        /// <summary>
        /// заявляем шине о том, какие события хотим прослушивать в этом медиаторе
        /// </summary>
        /// <returns></returns>
        public override string[] ListNotificationInterests()
        {
            return new[]
            {
                GameMessages.NOTE_SHIP_MOVE,
                GameMessages.NOTE_SHIP_SHOOT
            };
        }

        /// <summary>
        /// обработчки сообщиний шины
        /// </summary>
        public override void HandleNotification(INotification notification)
        {
            switch (notification.Name)
            {
                case GameMessages.NOTE_SHIP_MOVE:
                    Move(notification.Body as MouseEventArgs);
                    break;
                case GameMessages.NOTE_SHIP_SHOOT:
                    Shoot();
                    break;

                default:
                    break;
            }
        }

    }
}
