﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsteroidGame;
using Asteroids.model;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.view
{
    /// <summary>
    /// Реализует параллакс в игре
    /// </summary>
    class MdParallax : Mediator
    {
        // название медиатора
        public const string CAPTION = "MdParallax";
        // сслка на игровой прокси
        private PrGame prGame;
        // буффер для рисования графики
        public BufferedGraphics buffer;

        public MdParallax(BufferedGraphics aBuffer) : base(CAPTION, aBuffer)
        {
            prGame = Facade.RetrieveProxy(PrGame.CAPTION) as PrGame;
            buffer = aBuffer;
        }

        /// <summary>
        /// Вызывается при регистрации медиатора
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            Star.qty = 0;
            for (int i = 0; i < PrGame.MAX_SMALL_STARS; i++)
            {
                prGame.objList.Add(new Star().Init());
            }

            prGame.objList.Add(new Planet().Init());
        }

        /// <summary>
        /// Вызывается при уничтожении медиатора
        /// </summary>
        public override void OnRemove()
        {
            base.OnRemove();
        }

        private void Draw()
        {

        }

        /// <summary>
        /// заявляем шине о том, какие события хотим прослушивать в этом медиаторе
        /// </summary>
        /// <returns></returns>
        public override string[] ListNotificationInterests()
        {
            return new[] { GameMessages.NOTE_DRAW };
        }

        /// <summary>
        /// обработчки сообщиний шины
        /// </summary>
        public override void HandleNotification(INotification notification)
        {
            switch (notification.Name)
            {
                case GameMessages.NOTE_DRAW:
                    Draw();
                    break;
                default:
                    break;
            }
        }

    }
}
