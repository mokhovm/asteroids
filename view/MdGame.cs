﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AsteroidGame;
using Asteroids.model;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.view
{
    /// <summary>
    /// Медиатор игры
    /// </summary>
    class MdGame : Mediator
    {
        // название медиатора
        public const string CAPTION = "MdGame";

        // ссылка на корабль
        public Ship Ship => prGame.ship;

        // сслыка на игровую форму
        private FrmGame form;
        // графический контекст
        private BufferedGraphicsContext context;
        // буффер для рисования графики
        public BufferedGraphics buffer;
        // сслка на прокси
        private PrGame prGame;


        public MdGame(object viewComponent = null) : base(CAPTION, viewComponent)
        {
            prGame = Facade.RetrieveProxy(PrGame.CAPTION) as PrGame;
            form = new FrmGame();
            form.Width = PrGame.DEF_WIDTH;
            form.Height = PrGame.DEF_HEIGHT;
            form.StartPosition = FormStartPosition.CenterScreen;

            context = BufferedGraphicsManager.Current;
            Graphics g = form.CreateGraphics();
            buffer = context.Allocate(g, new Rectangle(0, 0, PrGame.DEF_WIDTH, PrGame.DEF_HEIGHT));
            
            form.Closed += (sender, args) => SendNotification(GameMessages.CMD_GAMESTOP, null, null); 
            form.Show();
        }


        /// <summary>
        /// Вызывается при регистрации медиатора
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            Facade.RegisterMediator(new MdHud(buffer));
            Facade.RegisterMediator(new MdParallax(buffer));
            Facade.RegisterMediator(new MdShip(buffer));
            Facade.RegisterMediator(new MdKeys(form));
            Facade.RegisterMediator(new MdScreenText(buffer));
            SendNotification(GameMessages.NOTE_SHOW_MSG, new ScreenTextVO("Ready!"), null);
        }

        /// <summary>
        /// Вызывается при уничтожении медиатора
        /// </summary>
        public override void OnRemove()
        {
            base.OnRemove();
            Facade.RemoveMediator(MdHud.CAPTION);
            Facade.RemoveMediator(MdParallax.CAPTION);
            Facade.RemoveMediator(MdShip.CAPTION);
            Facade.RemoveMediator(MdKeys.CAPTION);
            Facade.RemoveMediator(MdScreenText.CAPTION);
        }

        /// <summary>
        /// прорисовка объектов в новой позиции
        /// </summary>
        public void Draw()
        {
            buffer.Graphics.Clear(Color.Black);
            foreach (BaseObject obj in prGame.objList) obj.Draw(buffer);
            SendNotification(GameMessages.NOTE_DRAW, null, null);
            buffer.Render();
        }

        /// <summary>
        /// заявляем шине о том, какие события хотим прослушивать в этом медиаторе
        /// </summary>
        /// <returns></returns>
        public override string[] ListNotificationInterests()
        {
            return new[]
            {
                GameMessages.NOTE_GAME_TICK,
                GameMessages.NOTE_OBJECT_DIE,
                GameMessages.NOTE_GAME_LOSS,
                GameMessages.NOTE_GAME_NEXT_WAVE
            };
        }

        /// <summary>
        /// обработчки сообщиний шины
        /// </summary>
        /// <param name="note"></param>
        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
                case GameMessages.NOTE_GAME_TICK:
                    Draw();
                    break;
                case GameMessages.NOTE_OBJECT_DIE:
                    if (note.Body == Ship)
                        SendNotification(GameMessages.NOTE_GAME_LOSS, null, null);
                    break;
                case GameMessages.NOTE_GAME_LOSS:
                    prGame.Stop();
                    MessageBox.Show($"Вы набрали очков: {prGame.score}", "Корабль разбился!");
                    form.Close();
                    break;
                case GameMessages.NOTE_GAME_NEXT_WAVE:
                    SendNotification(GameMessages.NOTE_SHOW_MSG, new ScreenTextVO("Next wave!"), null);
                    break;
                default:
                    break;
            }

        }
    }
}
