﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Asteroids.model;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.view
{
    class MdMenu : Mediator
    {
        // название медиатора
        public const string CAPTION = "MdMenu";
        // показать эту форму
        public const string NOTE_FORM_SHOW = CAPTION + "/note/show";
        // ссылка на форму
        public FrmMenu Form => ViewComponent as FrmMenu;

        public MdMenu(object viewComponent) : base(CAPTION, viewComponent)
        {
            Form.btnStart.Click += (sender, args) =>SendNotification(GameMessages.CMD_GAMESTART, null, null);
            //Form.btnScores.Click += (sender, args) => SendNotification(AsteroidFacade.CMD_GAMESTART, null, null);
            Form.btnExit.Click += (sender, args) => Form.Close() ;
            Form.Closed += (sender, args) => SendNotification(GameMessages.CMD_SHUTDOWN, null, null);
            Form.StartPosition = FormStartPosition.CenterScreen;
        }

        /// <summary>
        /// заявляем шине о том, какие события хотим прослушивать в этом медиаторе
        /// </summary>
        /// <returns></returns>
        public override string[] ListNotificationInterests()
        {
            return new[]
            {
                NOTE_FORM_SHOW,
                GameMessages.CMD_SHUTDOWN,
                GameMessages.CMD_GAMESTART,
                GameMessages.CMD_GAMESTOP
            };
        }

        /// <summary>
        /// обработчки сообщиний шины
        /// </summary>
        public override void HandleNotification(INotification notification)
        {
            switch (notification.Name)
            {
                case NOTE_FORM_SHOW:
                    Form.Show();
                    break;
                case GameMessages.CMD_GAMESTART:
                    Form.Hide();
                    break;
                case GameMessages.CMD_GAMESTOP:
                    Form.Show();
                    break;
                case GameMessages.CMD_SHUTDOWN:
                    
                    break;
                
                default:
                    break;
            }

        }
    }
}
