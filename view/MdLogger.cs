﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Asteroids.model;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

namespace Asteroids.view
{
    /// <summary>
    /// используется для логгирования
    /// </summary>
    class MdLogger : Mediator
    {
        /// <summary>
        /// название медиатора
        /// </summary>
        public const string CAPTION = "MdLogger";
        /// <summary>
        /// имя лог-файла
        /// </summary>
        public const string FILE_NAME = "logfile.txt";

        public MdLogger() : base(CAPTION, null)
        {
        }

        public override void OnRegister()
        {
            base.OnRegister();
            AddToLog("logger activated");
        }

        /// <summary>
        /// добавляем сообщение в лог
        /// </summary>
        /// <param name="msg"></param>
        public void AddToLog(string msg)
        {
            msg = $"{DateTime.Now} {msg}";
            System.Diagnostics.Debug.WriteLine(msg);
            using (var file = new System.IO.StreamWriter(FILE_NAME, true))
            {
                file.WriteLine(msg);
            }
        }

        /// <summary>
        /// заявляем шине о том, какие события хотим прослушивать в этом медиаторе
        /// </summary>
        /// <returns></returns>
        public override string[] ListNotificationInterests()
        {
            return new[]
            {
                GameMessages.NOTE_LOG
            };
        }

        /// <summary>
        /// обработчки сообщиний шины
        /// </summary>
        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
                case GameMessages.NOTE_LOG:
                    if (note.Body is string s) AddToLog(s);
                    break;
                default:
                    break;
            }
        }
    }
}
