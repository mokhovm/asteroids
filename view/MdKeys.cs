﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Asteroids.model;
using PureMVC.Patterns.Mediator;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.view
{
    /// <summary>
    /// управляет работой контроллера ввода информации
    /// </summary>
    class MdKeys : Mediator
    {
        // название медиатора
        public const string CAPTION = "MdKeys";

        // ссылка на игровой прокси
        private PrGame prGame;
        // ссылка на форму, которая является источником клавиатурных сообщений
        private Form form;

        public MdKeys(Form aForm) : base(CAPTION, aForm)
        {
            prGame = Facade.RetrieveProxy(PrGame.CAPTION) as PrGame;
            form = aForm;
        }

        /// <summary>
        /// Вызывается при регистрации медиатора
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            form.KeyDown += OnKeyDown;
            form.MouseClick += OnMouseClick;
            form.MouseMove += OnMouseMove;
        }

        /// <summary>
        /// Вызывается при уничтожении медиатора
        /// </summary>
        public override void OnRemove()
        {
            base.OnRemove();
            form.KeyDown -= OnKeyDown;
            form.MouseClick -= OnMouseClick;
            form.MouseMove -= OnMouseMove;
        }

        /// <summary>
        /// вызывается при клике мышкой
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            SendNotification(GameMessages.NOTE_LOG,$"click at {e.X}:{e.Y}", null);
        }

        /// <summary>
        /// вызывается при перемещении мышки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                SendNotification(GameMessages.NOTE_SHIP_MOVE, e, null);
        }

        /// <summary>
        /// вызывается при нажатии клавиши
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.ControlKey)
            {
                SendNotification(GameMessages.NOTE_SHIP_SHOOT, null, null);
            }
        }

    }
}
