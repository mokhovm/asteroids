﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.model;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.view
{
    /// <summary>
    /// Управляет работой head-up дисплея
    /// </summary>
    class MdHud : Mediator
    {
        // название медиатора
        public const string CAPTION = "MdHud";

        // ссылка на игровой прокси
        private readonly PrGame prGame;

        // буффер для рисования графики
        public BufferedGraphics buffer;

        public MdHud(BufferedGraphics aBuffer) : base(CAPTION, aBuffer)
        {
            prGame = Facade.RetrieveProxy(PrGame.CAPTION) as PrGame;
            buffer = aBuffer;
        }

        /// <summary>
        /// Рисуем head-up дисплей
        /// </summary>
        private void Draw()
        {
            // размер шрифта
            const int EmSize = 12;
            // название шрифта
            const string FamilyName = "Arial";
            // высота HUD
            const int RectHeight = 20;
            // координаты показателей
            const int X1 = 10;
            const int X2 = 200;
            const int Y = 1;

            Brush brush = new SolidBrush(Color.FromArgb(128, 128, 128, 128));
            buffer.Graphics.FillRectangle(brush, 0, 0, PrGame.DEF_WIDTH, RectHeight);

            Font font = new Font(FamilyName, EmSize);
            brush = new SolidBrush(Color.Wheat);
            
            buffer.Graphics.DrawString($"Heath:{prGame.ship?.health} Score:{prGame.score}", font, brush, new PointF(X1, Y));
            buffer.Graphics.DrawString("Hold the left mouse button to control the ship and CTRL for fire!", font, brush, new PointF(X2, Y));
        }

        /// <summary>
        /// заявляем шине о том, какие события хотим прослушивать в этом медиаторе
        /// </summary>
        /// <returns></returns>
        public override string[] ListNotificationInterests()
        {
            return new[] {GameMessages.NOTE_DRAW};
        }

        /// <summary>
        /// обработчки сообщиний шины
        /// </summary>
        public override void HandleNotification(INotification notification)
        {
            switch (notification.Name)
            {
                case GameMessages.NOTE_DRAW:
                    Draw();
                    break;
                default:
                    break;
            }
        }

    }
}
