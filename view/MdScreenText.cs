﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.model;
using PureMVC.Interfaces;
using PureMVC.Patterns.Mediator;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids.view
{

    /// <summary>
    /// Управляет сообщениями на экране
    /// </summary>
    class MdScreenText : Mediator
    {
        // название медиатора
        public const string CAPTION = "MdScreenText";
        // ссылка на игровой прокси
        private PrGame prGame;
        // буффер для рисования графики
        public BufferedGraphics buffer;
        // список сообщений для показа
        private List<ScreenTextVO> jobs;

        public MdScreenText(BufferedGraphics aBuffer) : base(CAPTION, aBuffer)
        {
            prGame = Facade.RetrieveProxy(PrGame.CAPTION) as PrGame;
            buffer = aBuffer;
        }

        /// <summary>
        /// Вызывается при регистрации медиатора
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();
            jobs = new List<ScreenTextVO>();
        }

        /// <summary>
        /// Вызывается при уничтожении медиатора
        /// </summary>
        public override void OnRemove()
        {
            base.OnRemove();
            jobs.Clear();

        }

        /// <summary>
        /// проверяет, не выполнены ли задачи
        /// </summary>
        private void checkJobs()
        {
            foreach (ScreenTextVO vo in jobs)
            {
                if (DateTime.Now > vo.StartTime.AddMilliseconds(vo.ms)) vo.forRemove = true;
            }

            List<ScreenTextVO> remList = jobs.FindAll(o => o.forRemove);
            foreach (var vo in remList)
            {
                jobs.Remove(vo);
            }
            remList.Clear();
        }

        /// <summary>
        /// рисуем сообщения на экране
        /// </summary>
        private void Draw()
        {
            if (jobs.Count > 0)
            {
                Font font = new Font("Arial", 25);
                SolidBrush brush = new SolidBrush(Color.Wheat);

                foreach (ScreenTextVO vo in jobs)
                {
                    String text = $"{vo.text}";
                    SizeF size = buffer.Graphics.MeasureString(text, font);
                    PointF coord = new PointF();
                    coord.X = vo.coord.X - size.Width / 2;
                    coord.Y = vo.coord.Y - size.Height / 2;
                    buffer.Graphics.DrawString(text, font, brush, coord);
                }
                checkJobs();

            }
        }

        /// <summary>
        /// добавить текст в список задач
        /// </summary>
        /// <param name="vo"></param>
        private void AddText(ScreenTextVO vo)
        {
            vo.StartTime = DateTime.Now;
            jobs.Add(vo);
        }


        /// <summary>
        /// заявляем шине о том, какие события хотим прослушивать в этом медиаторе
        /// </summary>
        /// <returns></returns>
        public override string[] ListNotificationInterests()
        {
            return new[]
            {
                GameMessages.NOTE_SHOW_MSG,
                GameMessages.NOTE_DRAW
            };
        }

        /// <summary>
        /// обработчки сообщиний шины
        /// </summary>
        public override void HandleNotification(INotification note)
        {
            switch (note.Name)
            {
                case GameMessages.NOTE_SHOW_MSG:
                    var vo = note.Body as ScreenTextVO;
                    if (vo != null) AddText(vo);
                    break;
                case GameMessages.NOTE_DRAW:
                    Draw();
                    break;
                default:
                    break;
            }

        }


    }
}
