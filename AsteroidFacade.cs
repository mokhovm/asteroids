﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asteroids.controller;
using Asteroids.model;
using PureMVC.Patterns.Facade;

#region fileinfo
/*
   
    homework for geekbrains.ru made by Mokhov Mikhail 
    mokhovm@gmail.com

*/
#endregion

namespace Asteroids
{
    /// <summary>
    /// Фасад игры
    /// </summary>
    class AsteroidFacade : Facade
    {
    
        // ссылка на фасад игры
        public static AsteroidFacade MyFacade => GetInstance(() => new AsteroidFacade()) as AsteroidFacade;

        // стартуем приложение
        public void Startup(object app)
        {
            SendNotification(GameMessages.CMD_STARTUP, app);
        }

        // инициализируем команды 
        protected override void InitializeController()
        {
            base.InitializeController();
            RegisterCommand(GameMessages.CMD_STARTUP, () => new CmdStartup());
            RegisterCommand(GameMessages.CMD_SHUTDOWN, () => new CmdShutdown());
            RegisterCommand(GameMessages.CMD_GAMESTART, () => new CmdStartGame());
            RegisterCommand(GameMessages.CMD_GAMESTOP, () => new CmdStopGame());
        }
    }
}
