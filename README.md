# README #

Игра Asteroids. ver 0.04
Выполнил Мохов Михаил.
mokhovm@gmail.com

Игра написана с использованием паттерна MVC и его практической реализации в виде библиотеки PureMVC http://puremvc.org/ 
Благодаря такому подходу удалось существенно упросить взаимодействие частей игры между собой, разнести логику игры и её 
экранное представление. Для управления компонентами игры применена общая шина данных, реализованная с помощью паттерна 
Observer, а простая адресация и собственная система нотификаций позволяют легко перенести модули игры на любой другой 
язык программирования, поддерживающий PureMVC. Все ключевые части игры регистрируются в специальном реестре и объединяются 
с помощью паттерна Facade. Из любой точки программы можно обратится к нужному медиатору, прокси или команде, имея только 
ссылку на интерфейс фасада, которая реализована с помощью синглтона.
